﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    private float moveSpeed = 5f;
    private Rigidbody2D rb;
    private Vector2 moveDirection;
    private GameObject target;
    private float currentTime;
    private float endTime = 3f;
    private bool reflected = false;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.Find("Player");
        Vector3 targetCenter = new Vector3(target.transform.position.x, target.transform.position.y + 0.45f, 0f);
        moveDirection = (targetCenter - transform.position).normalized * moveSpeed;
        transform.Rotate(new Vector3(0f, 0f, Mathf.Rad2Deg * Mathf.Atan2(moveDirection.y, moveDirection.x)));
        rb.velocity = new Vector2(moveDirection.x, moveDirection.y);
        currentTime = 0f;
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= endTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Shield") && !reflected)
        {
            currentTime = 0f;
            moveDirection = new Vector2(0f, 0f) - rb.velocity;
            rb.velocity = moveDirection;
            transform.Rotate(0f, 0f, 180f);
            reflected = true;
        }
        if (collision.gameObject.CompareTag("Player") && !reflected)
        {
            target.GetComponent<PlayerPlatformerController>().health += -1;
            target.GetComponent<PlayerPlatformerController>().SetHealthText();
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Enemy") && reflected)
        {
            Destroy(collision.gameObject);
            
            target.GetComponent<PlayerPlatformerController>().score += 1;
            target.GetComponent<PlayerPlatformerController>().SetScoreText();
        }
        if (collision.gameObject.CompareTag("Ground"))
        {
            Destroy(gameObject);
        }
    }
}

// Code from Alexander Zotov: https://www.youtube.com/watch?v=kOzhE3_P2Mk